#!/usr/bin/env python
# coding=utf-8

"""
main.py
File to be executed to run main program, ie. solve for results and save the
data as pickle files in folder temp.
"""

from argparse import ArgumentParser

from Settings import Parameters, ProgramSettings
import Solvers

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "24 Jun 2016"


def main():
    parser = ArgumentParser()
    parser.add_argument(
        '-s', '--settings_file_suffix', type=str, default='')
    args = parser.parse_args()

    b = ProgramSettings(args.settings_file_suffix)
    p = Parameters(b)

    b.display_system_parameters(p)

    if b.steady_state:
        steady_state = Solvers.single_steady_state(b, p)
        Solvers.show_single_steady_state(b, p, steady_state)

    if b.time_evolution:
        time_evolution = Solvers.time_evolution(b, p)
        b.write_out(b.label_time_evolution, (p, time_evolution))

    if b.population_distribution:
        steady_states = Solvers.spectrum(b, p)
        b.write_out(b.label_spectrum, (p, steady_states))

    if b.pumping_response:
        solution = Solvers.pumping_response(b, p)
        b.write_out(b.label_pump_response, (p, solution))

    if b.diffusion_response:
        solution = Solvers.diffusion_response(b, p)
        b.write_out(b.label_diffusion, (p, solution))

    if b.two_d_map:
        solution = Solvers.polarisation_against_polarisation_and_pumping(b, p)
        b.write_out(b.label_2d_map, (p, solution))

    b.completion_dialogue()


if __name__ == '__main__':
    main()
