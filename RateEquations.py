#!/usr/bin/env python
# coding=utf-8

"""
RateEquations.py
File containing functions to define the differential equations (rate
equations).
"""

from math import pi, sqrt


def rate_equations(variables, t, p, gamma_up, pump_polarisation,
                   diffusion_coefficient, photon_modes):
    def photon_mode_sigma(i):
        return {1: 'y', 0: 'x'}[i % 2]

    def sign(sigma):
        return {'x': 1, 'y': -1}[sigma]

    def photon_mode(i):
        return variables[i]

    def excited_molecule(i, photon_modes_):
        return variables[p.total_number_of_modes(photon_modes_) + i]

    def excited_molecule_lm(l, m, photon_modes_):
        return variables[p.molecule_index_dict(photon_modes_)[(l, m)]]

    def photon_mode_rate_equation(i, photon_modes_):

        cap_n_terms_in_high_l = \
            0 if p.l_max == 0 \
                else -excited_molecule(2, photon_modes_) / sqrt(5) + \
                     sign(photon_mode_sigma(i)) * sqrt(3 / 10) * (
                         excited_molecule(3, photon_modes_) +
                         excited_molecule(1, photon_modes_))

        cap_n_tilde = excited_molecule(0, photon_modes_) + \
                      cap_n_terms_in_high_l

        expression = - p.kappa * photon_mode(i) + (4 * pi / 3) * (
            (p.gamma_minus(i) * (photon_mode(i) + 1) * cap_n_tilde) -
            p.gamma_plus(i) * photon_mode(i) * (
                p.capital_n - cap_n_tilde))

        return expression

    def excited_molecules_rate_equation(i, pump_polarisation_, photon_modes_):
        l, m = p.inverse_molecule_index(i)

        def zeta(sigma, photon_modes__):
            def numeric_term(photon_modes___):
                def integral_term(m2, photon_modes____):
                    if abs(m2) > p.l_max:
                        return 0
                    else:
                        return sum(
                            [excited_molecule_lm(
                                l_prime, m2, photon_modes____) *
                             p.integral_values_dictionary[
                                 (l_prime, m2, l, m)]
                             for l_prime in range(
                                abs(m2), p.l_max + 1, 2)])

                return pi * p.cap_a[(l, m)] * (
                    integral_term(m - 2, photon_modes___) +
                    integral_term(m + 2, photon_modes___))

            def analytic_term(photon_modes___):
                def term_l_m(photon_modes____):
                    coefficient = (1 - (((l - m + 1) * (l + m + 1)) / (
                        (2 * l + 1) * (2 * l + 3))) - (
                                       ((l + m) * (l - m)) / (
                                           (2 * l + 1) * (2 * l - 1))))
                    return coefficient * \
                           excited_molecule_lm(l, m, photon_modes____)

                def term_l_plus_2_m(photon_modes____):
                    coefficient = -1 / (2 * l + 3) * sqrt(
                        ((l - m + 2) * (l - m + 1) * (l + m + 2) *
                         (l + m + 1)) / ((2 * l + 1) * (2 * l + 5)))
                    new_l = l + 2
                    return coefficient * \
                           excited_molecule_lm(new_l, m, photon_modes____) \
                        if new_l <= p.l_max else 0

                def term_l_minus_2_m(photon_modes____):
                    coefficient = ((-1 / (2 * l - 1)) * sqrt(
                        ((l - m) * (l - m - 1) * (l + m) * (l + m - 1)) / (
                            (2 * l + 1) * (2 * l - 3))))
                    new_l = l - 2
                    return coefficient * \
                           excited_molecule_lm(new_l, m, photon_modes____) \
                        if new_l >= 0 and abs(m) <= new_l else 0

                return term_l_m(photon_modes___) + \
                       term_l_plus_2_m(photon_modes___) + \
                       term_l_minus_2_m(photon_modes___)

            return 0.5 * (sign(sigma) * numeric_term(photon_modes__) +
                          analytic_term(photon_modes__))

        def cap_n_minus_cap_n_up_term(l_, m_, sigma, photon_modes__):
            if not l_ and not m_:
                kronecker_delta_term = sqrt(10)
            elif l_ == 2:
                if not m_:
                    kronecker_delta_term = - sqrt(2)
                elif m_ in (2, -2):
                    kronecker_delta_term = sign(sigma) * sqrt(3)
            else:
                kronecker_delta_term = 0
            return ((p.capital_n / (3 * sqrt(10))) * kronecker_delta_term) - \
                   zeta(sigma, photon_modes__)

        def emission(i_, photon_modes__):
            return - (p.photon_mode_a(i_) + 1) * \
                   p.gamma_minus(i_) * (photon_mode(i_) + 1) * \
                   zeta(photon_mode_sigma(i_), photon_modes__)

        def absorption(i_, photon_modes__):
            return (p.photon_mode_a(i_) + 1) * \
                   p.gamma_plus(i_) * photon_mode(i_) * \
                   cap_n_minus_cap_n_up_term(
                       l, m, photon_mode_sigma(i_), photon_modes__)

        def diffusion(photon_modes__):
            return - diffusion_coefficient * l * (l + 1) * \
                   excited_molecule_lm(l, m, photon_modes__)

        def fluorescence(photon_modes__):
            return - p.gamma_down * excited_molecule_lm(l, m, photon_modes__)

        def pump(pump_polarisation__, photon_modes__):
            def pumping(sigma):
                return gamma_up * \
                       cap_n_minus_cap_n_up_term(l, m, sigma, photon_modes__)

            x, y = pump_polarisation__

            return x * pumping('x') + y * pumping('y')

        return pump(pump_polarisation_, photon_modes_) + \
               fluorescence(photon_modes_) + diffusion(photon_modes_) + \
               sum([absorption(i, photon_modes_) + emission(i, photon_modes_)
                    for i in range(0, p.total_number_of_modes(photon_modes_))])

    return [photon_mode_rate_equation(i, photon_modes)
            for i in range(0, p.total_number_of_modes(photon_modes))] + \
           [excited_molecules_rate_equation(i, pump_polarisation, photon_modes)
            for i in range(0, p.number_of_molecular_l_m_states())]


if __name__ == '__main__':
    exit()
