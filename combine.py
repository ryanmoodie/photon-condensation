#!/usr/bin/env python
# coding=utf-8

"""
combine.py
File to be executed to combine separate results into one pickle file. 
"""

from numpy import array_equal, concatenate

from Settings import ProgramSettings

NAME0 = 'june_combined'
REVERSE0 = False
NAME1 = 'june_3'
REVERSE1 = False
TYPE = 'pumping_response'
# TYPE = 'diffusion'
CHANGES = ['gamma_up_log_list']
# CHANGES = ['diffusion_list_log']


def merger(differences, p, p0, p1, dir0, dir1):
    print('Differences:')
    for attr in differences:
        print(f'  {attr}: {getattr(p0, attr)} != {getattr(p1, attr)}')
        try:
            if True:
                setattr(p, attr,
                        concatenate((getattr(p0, attr)[::dir0],
                                     getattr(p1, attr)[::dir1])))
            else:
                setattr(p, attr,
                        getattr(p0, attr)[::dir0] + getattr(p1, attr)[::dir1])
        except TypeError:
            setattr(p, attr, getattr(p0, attr) + getattr(p1, attr))
        print(f'    Fixed -> {getattr(p, attr)}')
    return p


def main():
    b = ProgramSettings()
    direction0 = -1 if REVERSE0 else 1
    direction1 = -1 if REVERSE1 else 1

    p0, solution0 = b.read_file(f'temp/{NAME0}_{TYPE}.pkl')
    # b.print_log('')
    p1, solution1 = b.read_file(f'temp/{NAME1}_{TYPE}.pkl')

    try:
        solution = solution0[::direction0] + solution1[::direction1]
    except ValueError:
        solution = concatenate(
            (solution0[::direction0], solution1[::direction1]))

    p = p0

    attributes = [a for a in dir(p)
                  if not a.startswith('__') and not callable(getattr(p, a))]

    if not CHANGES:

        differences = []

        for attr in attributes:
            try:
                if 'numpy.ndarray' in str(type(getattr(p, attr))):
                    if not array_equal(getattr(p0, attr), getattr(p1, attr)):
                        differences.append(attr)
                elif getattr(p0, attr) != getattr(p1, attr):
                    differences.append(attr)
            except AttributeError:
                print('Warning: non-compatibility!')

        p = merger(differences, p, p0, p1, direction0, direction1)

    else:

        p = merger(CHANGES, p, p0, p1, direction0, direction1)

    b.write_out(f'temp/{NAME0}_combined_{TYPE}.pkl', (p, solution))


if __name__ == '__main__':
    main()
