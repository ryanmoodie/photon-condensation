#!/usr/bin/env python
# coding=utf-8

from argparse import ArgumentParser
from os import system

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "22 Sep 2017"


def main():
    parser = ArgumentParser()
    parser.add_argument('-r', '--file_to_run', type=str, default='plot',
                        help='main or plot')
    args = parser.parse_args()

    queue = [
        '_normal_kappa',
        '_small_kappa',
    ]

    for i, entry in enumerate(queue, 1):
        print(f'Starting {i} of {len(queue)}: {entry}\n')
        system(f'python {args.file_to_run}.py -s {entry}')
        print(f'Done {i} of {len(queue)}\n')


if __name__ == "__main__":
    main()
