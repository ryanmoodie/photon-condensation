#!/usr/bin/env python
# coding=utf-8

"""
Core.py
File containing functions to solve for the steady state.
"""

from time import clock

from numpy import linspace
from scipy.integrate import odeint
from scipy.optimize import root

from RateEquations import rate_equations

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "26 Jul 2016"


def integrator(
        b, p, start_time, stop_time, number_of_points, initial_state,
        gamma_up, pump_polarisation, diffusion_coefficient, photon_modes):
    times = linspace(start_time, stop_time, number_of_points)

    dynamics = odeint(
        rate_equations, initial_state, times,
        (p, gamma_up, pump_polarisation, diffusion_coefficient, photon_modes),
        atol=b.integration_tolerance, rtol=b.integration_tolerance)

    return dynamics


def root_finder(b, p, gamma_up, initial_variables, pump_polarisation,
                diffusion_coefficient, photon_modes, i=0, total=1):
    if total > 1:
        sweep_iteration_start_time = clock()
        current_name = f'Sweep iteration {i + 1} of {total} ' \
                       f'({round(100*(i+1)/total, 1)}%): '
        b.print_log(f'\n{current_name}calculating...')

    b.print_log('  Finding steady state by root...')

    root_finder_start_time = clock()

    solution = root(
        rate_equations, initial_variables, (
            0., p, gamma_up, pump_polarisation, diffusion_coefficient,
            photon_modes), method='lm',
        options={'xtol': b.tolerance, 'ftol': b.tolerance})

    answer = solution.x

    b.print_log(f'    Found roots ({b.stopwatch(root_finder_start_time)}).\n'
                f'  Checking solution...')

    if b.cut_off_root_finder_at_zero:
        answer[:p.total_number_of_modes(photon_modes)] = \
            [0 if x < 0 else x
             for x in answer[:p.total_number_of_modes(photon_modes)]]
        if any([x < 0
                for x in answer[:p.total_number_of_modes(photon_modes)]]):
            b.print_log('Warning: had negative photon population')

    if b.check_physicality and ((not solution.success) or any(
            [x < -b.zero
             for x in answer[:p.total_number_of_modes(photon_modes)]])):
        reason = 'physicality check' if solution.success else 'root check'
        b.print_log(f'    Failed {reason}.\n'
                    f'      Falling back to iterative integrator:')
        answer = iterative_integrator(
            b, p, gamma_up, initial_variables, pump_polarisation,
            diffusion_coefficient, photon_modes, 0, 1)
    else:
        b.print_log('    Success.')

    if total > 1:
        percentage = round(
            100 * b.raw_stopwatch(sweep_iteration_start_time) /
            b.raw_stopwatch(b.program_start_time), 1)

        b.print_log(
            f'{current_name}Done ('
            f'{b.stopwatch(sweep_iteration_start_time)} '
            f'({percentage}%) of '
            f'{b.stopwatch(b.program_start_time)}).')

    return answer


def iterative_integrator(b, p, gamma_up, initial_variables, pump_polarisation,
                         diffusion_coefficient, photon_modes, i=0, total=1):
    def integrate_one_time_step(time_, old_solution_, i_):
        iteration_time = clock()
        b.print_log(f'    Iteration {i_ + 1}...')

        solution = integrator(
            b, p, time_, time_ + b.integrate_to_steady_step, 2,
            old_solution_, gamma_up, pump_polarisation,
            diffusion_coefficient, photon_modes)[-1]

        if b.cut_off_integrator_at_zero:
            solution[:p.total_number_of_modes(photon_modes)] = \
                [0 if x < 0 else x
                 for x in solution[:p.total_number_of_modes(photon_modes)]]
            if any([x < 0 for x in
                    solution[:p.total_number_of_modes(photon_modes)]]):
                b.print_log('Warning: had negative photon population')

        b.print_log(
            f'      Done. ({b.stopwatch(iteration_time)})')

        return time_ + b.integrate_to_steady_step, solution, \
               old_solution_, i_ + 1

    def convergence_test(old_pop, new_pop):
        delta = abs(new_pop - old_pop)
        if delta < b.zero:
            return False
        else:
            return delta > (b.integrate_to_steady_precision *
                            min(abs(new_pop), abs(old_pop)))

    if total > 1:
        sweep_iteration_start_time = clock()
        current_name = f'Sweep iteration {i + 1} of {total} ' \
                       f'({round(100*(i+1)/total,1)}%): '
        b.print_log(f'\n{current_name}calculating...')

    start_time = clock()

    b.print_log(
        '  Looking for steady state by integrating iteratively...')

    time = 0
    i = 0

    time, new_solution, old_solution, i = \
        integrate_one_time_step(time, initial_variables, i)

    while any([convergence_test(old_pop, new_pop)
               for new_pop, old_pop in zip(new_solution, old_solution)]):
        time, new_solution, old_solution, i = \
            integrate_one_time_step(time, new_solution, i)

    b.print_log(f'  Converged after {i} iterations ('
                f'{b.stopwatch(start_time)}).')

    if total > 1:
        b.print_log(
            f'{current_name}Done ('
            f'{b.stopwatch(sweep_iteration_start_time)} of '
            f'{b.stopwatch(b.program_start_time)}).')

    return new_solution


if __name__ == '__main__':
    exit()
