#!/usr/bin/env python
# coding=utf-8

"""
Solvers.py
File containing functions to solve the differential equations (rate
equations).
"""

from multiprocessing import Pool
from time import clock

from numpy import empty

from Core import integrator, root_finder, iterative_integrator

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "26 Jul 2016"


def time_evolution(b, p):
    start_time = clock()
    b.print_log('\nIntegrating rate equations to find time evolution...')

    solution = integrator(
        b, p, 0, p.stop_time, p.number_of_points,
        p.initial_state(p.number_of_modes), p.gamma_up, p.input_polarisation,
        p.capital_d, p.number_of_modes)

    b.print_log(f'  Integration complete ({b.stopwatch(start_time)}).\n')

    return solution


def single_steady_state(b, p):
    start_time = clock()
    if p.pump_polarisation_definition == 'angle':
        polarisation = f'{p.pump_polarisation_angle} degrees.'
    elif p.pump_polarisation_definition == 'degree':
        polarisation = f'degree {p.pump_polarisation_degree}.'
    b.print_log(
        f'\nSolving for steady state for gamma_up = {p.gamma_up}'
        f' GHz, D = {p.capital_d} GHz and pump polarisation of {polarisation}')

    if False:
        steady_state = root_finder(
            b, p, p.gamma_up, p.initial_state(p.number_of_modes),
            p.input_polarisation, p.capital_d, p.number_of_modes)
    else:
        steady_state = iterative_integrator(
            b, p, p.gamma_up, p.initial_state(p.number_of_modes),
            p.input_polarisation, p.capital_d, p.number_of_modes)

    b.print_log(
        f'Calculation complete ({b.stopwatch(start_time)}).\n')

    return steady_state


def spectrum(b, p):
    start_time = clock()
    b.print_log('Solving for photon spectrum...')

    with Pool(processes=b.number_of_workers) as w:
        steady_states = w.starmap(
            iterative_integrator, [
                (b, p, gamma_up, p.initial_state(p.spectrum_modes),
                 p.input_polarisation, p.capital_d, p.spectrum_modes, i,
                 len(p.spectrum_gamma_up_list))
                for i, gamma_up in enumerate(p.spectrum_gamma_up_list)])

    b.print_log(
        f'Calculation complete ({b.stopwatch(start_time)}).\n')

    return steady_states


def pumping_response(b, p):
    b.print_log('\nCalculating: response to pumping...')

    solution = p.initial_state(p.number_of_modes)

    all_solutions = empty(shape=(len(p.gamma_up_log_list), len(solution)))

    if p.input_initial_state == 'empty':

        solution = iterative_integrator(
            b, p, p.gamma_up_log_list[0], solution, p.input_polarisation,
            p.capital_d, p.number_of_modes, 0, len(p.gamma_up_log_list))

    else:

        solution = root_finder(
            b, p, p.gamma_up_log_list[0], solution, p.input_polarisation,
            p.capital_d, p.number_of_modes, 0, len(p.gamma_up_log_list))

    all_solutions[0, :] = solution

    for i, gamma_up in enumerate(p.gamma_up_log_list[1:], start=1):
        solution = root_finder(
            b, p, gamma_up, solution, p.input_polarisation,
            p.capital_d, p.number_of_modes, i, len(p.gamma_up_log_list))

        all_solutions[i, :] = solution

    b.print_log('')

    return all_solutions


def single_diffusion_response(b, p, i, gamma_up, number):
    solution = p.initial_state(p.number_of_modes)

    diffusion_list = p.diffusion_list

    all_solutions = empty(shape=(len(diffusion_list), len(solution)))

    if p.input_initial_state == 'empty':

        solution = iterative_integrator(
            b, p, gamma_up, solution, p.input_polarisation, diffusion_list[0],
            p.number_of_modes, i * len(diffusion_list),
                               number * len(diffusion_list))

    else:

        solution = root_finder(
            b, p, gamma_up, solution, p.input_polarisation,
            diffusion_list[0], p.number_of_modes, i * len(diffusion_list),
                                                  number * len(diffusion_list))

    all_solutions[0, :] = solution

    for j, diffusion_coefficient in enumerate(diffusion_list[1:], start=1):
        solution = root_finder(
            b, p, gamma_up, solution, p.input_polarisation,
            diffusion_coefficient, p.number_of_modes,
            i * len(diffusion_list) + j, number * len(diffusion_list))

        all_solutions[j, :] = solution

    b.print_log('')

    return all_solutions


def diffusion_response(b, p):
    b.print_log('\nCalculating: response to diffusion...')

    number = len(p.gamma_up_diffusion_list)

    with Pool(processes=b.number_of_workers) as w:
        solutions = w.starmap(
            single_diffusion_response,
            [(b, p, i, gamma_up, number)
             for i, gamma_up in enumerate(p.gamma_up_diffusion_list)])

    return solutions


def find_line_polarisation(b, p, i, input_polarisation):
    solution = p.initial_state(p.number_of_modes)

    line = empty(shape=(len(p.gamma_up_linear_list), len(solution)))

    total = len(p.input_polarisation_list_half) * len(p.gamma_up_linear_list)

    solution = iterative_integrator(
        b, p, p.gamma_up_linear_list[0], solution, input_polarisation,
        p.capital_d, p.number_of_modes, i * len(p.gamma_up_linear_list), total)

    line[0, :] = solution

    for j, gamma_up in enumerate(p.gamma_up_linear_list[1:], start=1):
        solution = root_finder(
            b, p, gamma_up, solution, input_polarisation, p.capital_d,
            p.number_of_modes, i * len(p.gamma_up_linear_list) + j, total)

        line[j, :] = solution

    return line


def polarisation_against_polarisation_and_pumping(b, p):
    b.print_log('\nCalculating: output polarisation response to '
                'input polarisation and pumping strength...')

    with Pool(processes=b.number_of_workers) as w:
        solution = w.starmap(
            find_line_polarisation,
            [(b, p, i, e) for i, e in
             enumerate(p.input_polarisation_list_half)])

    b.print_log(
        '\nFinished calculating output polarisation response to '
        'input polarisation and pumping strength.\n')

    return solution


def show_single_steady_state(b, p, steady_state_):
    b.print_log(
        f'Steady state is :\n{list(steady_state_)}\n'
        f'P: {p.find_output_polarisation_degree(steady_state_)}')


if __name__ == '__main__':
    exit()
