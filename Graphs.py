#!/usr/bin/env python
# coding=utf-8

"""
Graphs.py
File containing functions to plot graphs of results.
"""

# sqrt important for eval function, even though not explicitly used!
from math import log10, floor, ceil, sqrt
from string import ascii_lowercase

from matplotlib.pyplot import tight_layout, subplots, tick_params, \
    NullFormatter
from matplotlib.style import use
from matplotlib.ticker import FormatStrFormatter, LogLocator, MaxNLocator
from numpy import linspace, arange, meshgrid
from scipy.constants import golden

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "24 Jun 2016"

colour_wheel = ['k', 'r', 'b', 'g', 'c', 'm', 'y']
style_wheel = ['-', '--', '-.', ':']


def _initialise_plot(gs, aspect=1 / golden, rows=1, cols=1):
    if gs.use_old_graph_aesthetic:
        use('classic')

    if gs.custom_graph_aspect:
        w = gs.figure_width
        h = w * aspect
        fig, axs = subplots(
            nrows=rows, ncols=cols, figsize=(w, h), num=gs.graph_number)
    else:
        fig, axs = subplots(
            nrows=rows, ncols=cols, num=gs.graph_number)

    gs.graph_number += 1

    return fig, axs


def _sign(sign):
    return {'+': 1, '-': -1}[sign]


def _colour_cycler(i):
    mode = int(_inverse_photon_mode_index(i)
               [0:len(_inverse_photon_mode_index(i)) - 1])
    return colour_wheel[mode % 7]


def _index_colour_cycler(i):
    return colour_wheel[i % 7]


def _index_line_style_cycler(i):
    return style_wheel[i % len(style_wheel)]


def _line_style_cycler(i):
    return {'x': '-', 'y': '--'}[_inverse_photon_mode_index(i)
    [len(_inverse_photon_mode_index(i)) - 1]]


def _molecular_index(p, i):
    l, m = p.inverse_molecule_index(i)
    return 'N_{' + f'{l}, {m}' + '}'


def _molecular_label(p, i):
    return f'${_molecular_index(p, i)}$'


def _extract_a_molecule_population(p, i, all_populations):
    return [row[i + p.total_number_of_modes(p.number_of_modes)]
            for row in all_populations]


def _inverse_photon_mode_index(i):
    return f'{(i - 1) // 2}y' if i % 2 else f'{i // 2}x'


def _inverse_photon_mode_label(i):
    photon_index_str = _inverse_photon_mode_index(i)
    photon_index = [photon_index_str[0:-1], photon_index_str[-1]]
    return '$n_{' + photon_index[0] + '}^{' \
           + photon_index[1] + photon_index[1] + '}$'


def _photon_mode_index(a, sigma):
    return {'x': 2 * int(a), 'y': 2 * int(a) + 1}[str(sigma)]


def time_evolution(b, gs, p, solution):
    # for sol in solution:
    #     print(list(sol))
    # print(solution[-1])
    # exit()

    def draw_titles(ax_, population):
        describer = {
            'n': 'photon mode populations',
            'N': 'excited molecular state\nin components of spherical '
                 'harmonic expansion',
            'p': 'polarisation'}[population]
        ax_.set_xlabel('$t$ (ns)')
        ax_.set_ylabel('Populations')
        ax_.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))
        if gs.draw_graph_titles:
            ax_.set_title('Time evolution of ' + describer)

    def plot_photon_mode(i_):
        if gs.linear_photons:
            ax_pho.plot(
                t, solution[:, i_],
                label=_inverse_photon_mode_label(i_),
                linestyle=_line_style_cycler(i_),
                color=_colour_cycler(i_))
        else:
            ax_pho.semilogy(
                t, solution[:, i_],
                label=_inverse_photon_mode_label(i_),
                linestyle=_line_style_cycler(i_),
                color=_colour_cycler(i_))
        ax_pho.legend(loc='best')

    def plot_molecule_population(i_):
        ax_mol.plot(
            t, solution[:, i_ + p.total_number_of_modes(p.number_of_modes)],
            label=_molecular_label(p, i_))
        ax_mol.legend(loc='best')

    b.print_log('\nPlotting time evolution...\n')

    t = linspace(0, p.stop_time, p.number_of_points)

    if 'polarisation' == gs.plot:
        polarisation = [
            p.find_output_polarisation_degree(point) for point in solution]
        fig_pol, ax_pol = _initialise_plot(gs, gs.aspect_time)
        draw_titles(ax_pol, 'p')
        ax_pol.plot(t, polarisation)
        ax_pol.set_ylabel('$P$')
        # ax_pol.ticklabel_format(axis='y', useOffset=False)
        return [(fig_pol, "polarisation_time_evolution")]

    if 'n' in gs.plot:
        fig_pho, ax_pho = _initialise_plot(gs, gs.aspect_time)
        draw_titles(ax_pho, 'n')

    if 'N' in gs.plot:
        fig_mol, ax_mol = _initialise_plot(gs, gs.aspect_time)
        draw_titles(ax_mol, 'N')

    if 'all' in gs.plot:
        if 'n' in gs.plot:
            for i in range(0, p.total_number_of_modes(p.number_of_modes)):
                plot_photon_mode(i)

        if 'N' in gs.plot:
            for i in range(0, p.number_of_molecular_l_m_states()):
                plot_molecule_population(i)

    else:

        for i in range(0, p.total_number_of_modes(p.number_of_modes)):
            if 'n' + _inverse_photon_mode_index(i) in gs.plot:
                plot_photon_mode(i)

        for i in range(0, p.number_of_molecular_l_m_states()):
            if _molecular_index(p, i) in gs.plot:
                plot_molecule_population(i)

    output = []

    if 'n' in gs.plot:
        output.append((fig_pho, "photon_mode_time_evolution"))

    if 'N' in gs.plot:
        output.append((fig_mol, "excited_state_time_evolution"))

    return output


def spectrum(b, gs, p, steady_states):
    b.print_log('\nPlotting photon spectrum...\n')

    number = len(steady_states)

    fig, axs = _initialise_plot(gs, number / (2 * golden), cols=number)

    for ax, steady_state in zip(axs, steady_states):

        populations = []

        for j, polarisation in enumerate(['x', 'y']):
            population = [(i_ + 1) * steady_state[2 * i_ + j]
                          for i_ in range(p.spectrum_modes)]

            populations += population

            ax.semilogy(
                arange(0, p.spectrum_modes), population,
                label='$n_{\mathfrak{m}}^{' + polarisation +
                      polarisation + '}$',
                color=_index_colour_cycler(j),
                linestyle=_index_line_style_cycler(j))

        ax.locator_params(axis='x', nbins=3)
        ax.set_xlabel('$\mathfrak{m}$')

        if gs.draw_graph_titles:
            ax.set_title('Populations of Photon Modes')

        ax.set_xlim(
            [-(p.spectrum_modes - 1) * 0.01,
             (p.spectrum_modes - 1) * 1.01])
        # ax.set_ylim([min(populations), max(populations) * 1.05])
        set_log_lims(ax, populations)

    axs[0].set_ylabel('$g_{\mathfrak{m}} n_{\mathfrak{m}}^{\sigma\sigma}$')
    axs[-1].legend(loc='upper right')

    tight_layout()

    return [(fig, "population_distribution")]


def set_log_lims(ax_, points, more_space=False):
    low_order = floor(log10(abs(min(points))))
    high_order = ceil(log10(abs(max(points))))
    lower_mod = 0.7 if more_space else 0.8
    upper_mod = 3 if more_space else 2
    ax_.set_ylim(lower_mod * 10 ** low_order, upper_mod * 10 ** high_order)


def pumping_response(b, gs, p, solutions):
    # print(list(solutions[-1]))
    # exit()
    x_axis = [x / p.gamma_down for x in p.gamma_up_log_list]

    def extract_a_photon_population(human_index):
        mode_number = human_index[:len(human_index) - 1]
        sigma_ = human_index[-1]
        return [row[_photon_mode_index(mode_number, sigma_)]
                for row in solutions]

    def mode_sum(sigma_):
        sigma_populations = [extract_a_photon_population(str(i) + sigma_)
                             for i in range(0, p.number_of_modes)]
        return [sum([row[i] for row in sigma_populations])
                for i in range(0, len(p.gamma_up_log_list))]

    def polarisation_degree(solutions_):
        return [p.find_output_polarisation_degree(solution)
                for solution in solutions_]

    def raw_plot_molecule_population(ax_mol_, population_, label_):
        if gs.pumping_log_scale_for_molecules:
            abs_population = [abs(element) for element in population_]
            ax_mol_.loglog(x_axis, abs_population, label=label_)
        else:
            ax_mol_.semilogx(x_axis, population_, label=label_)

    def raw_pretty_plot_molecule_population(
            ax_mol_, population_, label_, i_):
        if gs.pumping_log_scale_for_molecules:
            abs_population = [abs(element) for element in population_]
            ax_mol_.loglog(
                x_axis, abs_population,
                label=label_, color=_index_colour_cycler(i_),
                linestyle=_index_line_style_cycler(i_))
        else:
            ax_mol_.semilogx(
                x_axis, population_,
                label=label_, color=_index_colour_cycler(i_),
                linestyle=_index_line_style_cycler(i_))
        return i_ + 1

    def plot_molecule_population(ax_mol_, i_):
        population_ = _extract_a_molecule_population(p, i_, solutions)
        if population_[0] < 0 and gs.pumping_log_scale_for_molecules:
            label_ = '$|' + _molecular_index(p, i_) + '|$'
        else:
            label_ = _molecular_label(p, i_)
        raw_plot_molecule_population(ax_mol_, population_, label_)

    def plot_pretty_molecule_population(ax_mol_, i_, j):
        population_ = _extract_a_molecule_population(p, i_, solutions)
        if population_[0] < 0 and gs.pumping_log_scale_for_molecules:
            label_ = '$|' + _molecular_index(p, i_) + '|$'
        else:
            label_ = _molecular_label(p, i_)
        j = raw_pretty_plot_molecule_population(
            ax_mol_, population_, label_, j)
        return j

    def plot_photon_population(ax_, population_, label_):
        if gs.linear_scale_for_photons or all(population_) == 0.:
            ax_.semilogx(x_axis, population_, label=label_)

        else:
            ax_.loglog(x_axis, population_, label=label_)

    def plot_pretty_photon_population_raw(ax, population_, label_, i):
        if gs.linear_scale_for_photons or all(population_) == 0.:
            ax.semilogx(
                x_axis, population_,
                label=label_, linestyle=_index_line_style_cycler(i),
                color=_index_colour_cycler(i))
        else:
            ax.loglog(
                x_axis, population_,
                label=label_, linestyle=_index_line_style_cycler(i),
                color=_index_colour_cycler(i))

    def plot_pretty_photon_population(population_, i):
        if gs.linear_scale_for_photons or all(population_) == 0.:
            ax_pho.semilogx(
                x_axis, population_,
                label=_inverse_photon_mode_label(i),
                linestyle=_line_style_cycler(i),
                color=_colour_cycler(i))
        else:
            ax_pho.loglog(
                x_axis, population_,
                label=_inverse_photon_mode_label(i),
                linestyle=_line_style_cycler(i),
                color=_colour_cycler(i))

    def draw_shared_titles(ax_):
        ax_.set_xlabel(r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$')
        ax_.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))

    def complete_graph(fig_, ax_, describer, name_):
        if gs.draw_graph_titles:
            ax_.set_title(describer + ' against pumping strength')
        ax_.set_xlim(x_axis[0], x_axis[-1])
        ax_.legend(loc='best')
        return fig_, name_

    b.print_log('\nPlotting pumping response...\n')

    if gs.paper_format_fig_3:

        n = 3 if gs.paper_polarisation else 2

        fig, axs = _initialise_plot(gs, golden * n / 3, rows=n)

        fig.subplots_adjust(hspace=0)

        for i, sigma in enumerate(('x', 'y')):
            plot_pretty_photon_population_raw(
                axs[0], mode_sum(sigma),
                '$n^{' + sigma + '}_{tot}$', i)

        set_log_lims(axs[0], mode_sum('x') + mode_sum('y'), more_space=True)

        mol_line_num = plot_pretty_molecule_population(axs[1], 0, 0)

        for number, label in \
                zip((2, 3), ('$|N_{2, 0}|$', '$N_{2, \pm 2}$')):

            popu = _extract_a_molecule_population(p, number, solutions)

            if label == '$|N_{2, 0}|$' and popu[0] > 0:
                exit("Positive population!"
                     "Remove modulus sign: Graphs.py, line 352")

            mol_line_num = raw_pretty_plot_molecule_population(
                axs[1], popu, label, mol_line_num)

        harmonics_raw = \
            [_extract_a_molecule_population(p, i, solutions)
             for i in (0, 2, 3)]

        harmonics = \
            [abs(element) for row in harmonics_raw for element in row]
        set_log_lims(axs[1], harmonics, more_space=True)

        axs[0].legend(loc='upper left')
        axs[1].legend(loc='upper left' if gs.paper_polarisation else 'lower left')

        for ax in axs:
            ax.set_xlim(x_axis[0], x_axis[-1])

        if gs.paper_polarisation:

            if gs.linear_scale_for_polarisation:

                axs[-1].locator_params(axis='y', nbins=3)
                axs[-1].semilogx(
                    x_axis, polarisation_degree(solutions),
                    label='$P_{out}$', color='k')

            else:

                axs[-1].loglog(
                    x_axis, polarisation_degree(solutions),
                    label='$P_{out}$', color='k')

            axs[-1].set_ylim(0, 1.1)

        axs[-1].set_xlabel('$\Gamma_\\uparrow/\Gamma_\\downarrow$')

        for ax, label in zip(axs, [
            '$n_{tot}^{\sigma}$', '$N_{l, m}$', '$P$']):
            ax.set_ylabel(label)

        tick_params(axis='x', which='minor')
        axs[-1].xaxis.set_minor_formatter(FormatStrFormatter("%.2f"))
        axs[-1].xaxis.set_major_formatter(FormatStrFormatter("%.2f"))

        for ax in axs:
            ax.xaxis.set_minor_locator(LogLocator(subs=[2, 4, 6, 8]))

        for ax in axs[:-1]:
            ax.xaxis.set_major_formatter(NullFormatter())
            ax.yaxis.set_minor_locator(LogLocator(subs=[2, 4, 6, 8]))

        return [(fig, 'pumping_response')]

    else:

        if gs.populations_to_show == 'polarisation':
            fig_pho, ax_pho = _initialise_plot(gs, 1 / golden)
            draw_shared_titles(ax_pho)
            ax_pho.set_ylabel('$P$')
            ax_pho.ticklabel_format(axis='y', useOffset=False)
            if gs.linear_scale_for_polarisation:
                ax_pho.semilogx(
                    x_axis, polarisation_degree(solutions),
                    label='$P_{out}$', color='r')
            else:
                ax_pho.loglog(
                    x_axis, polarisation_degree(solutions),
                    label='$P_{out}$', color='r')

            return [complete_graph(
                fig_pho, ax_pho, 'Polarisation',
                "pumping_response_polarisation")]

        if 'n' in gs.populations_to_show:
            fig_pho, ax_pho = _initialise_plot(gs, 1 / golden)
            draw_shared_titles(ax_pho)
            ax_pho.set_ylabel('$n_{\mathfrak{m}}^{\sigma\sigma}$')

        if 'N' in gs.populations_to_show:
            fig_mol, ax_mol = _initialise_plot(gs, 1 / golden)
            draw_shared_titles(ax_mol)
            ax_mol.set_ylabel('$N_{l, m}$')

        if gs.populations_to_show == 'sum_n':
            population = [mode_sum('x')[i] + mode_sum('y')[i]
                          for i in range(0, len(p.gamma_up_log_list))]

            plot_photon_population(ax_pho, population, 'All n summed')

            describer_pho = 'All photon modes summed'

        elif gs.populations_to_show == 'sum_N':
            populations = [
                _extract_a_molecule_population(p, i, solutions)
                for i in range(0, p.number_of_molecular_l_m_states())]

            summed = [sum([row[i] for row in populations])
                      for i in range(0, len(p.gamma_up_log_list))]

            raw_plot_molecule_population(ax_mol, summed, 'All N summed')

            describer_mol = 'All molecular populations summed'

        elif gs.populations_to_show == 'sum_sigma_n':
            plot_photon_population(ax_pho, mode_sum('x'), '$n^{x}_{tot}$')
            plot_photon_population(ax_pho, mode_sum('y'), '$n^{y}_{tot}$')

            describer_pho = 'Photon modes of same polarisation summed'

        elif gs.populations_to_show == 'all_n':
            for i in range(0, p.total_number_of_modes(p.number_of_modes)):
                population = [row[i] for row in solutions]
                plot_pretty_photon_population(population, i)

            describer_pho = 'Photon modes'

        elif gs.populations_to_show == 'all_N':
            for i in range(0, p.number_of_molecular_l_m_states()):
                plot_molecule_population(ax_mol, i)

            describer_mol = 'Molecular populations'

        elif gs.populations_to_show == 'all_n_N':
            for i in range(0, p.total_number_of_modes(p.number_of_modes)):
                population = [row[i] for row in solutions]
                plot_pretty_photon_population(
                    population, i)

            describer_pho = 'Photon modes'

            for i in range(0, p.number_of_molecular_l_m_states()):
                plot_molecule_population(ax_mol, i)

            describer_mol = 'Molecular populations'

        else:

            for i in range(0, p.total_number_of_modes(p.number_of_modes)):
                index = _inverse_photon_mode_index(i)
                name = f'n{index}'
                if name in gs.populations_to_show:
                    plot_pretty_photon_population(
                        extract_a_photon_population(index), i)

            describer_pho = 'Photon modes'

            for i in range(0, p.number_of_molecular_l_m_states()):
                index = _molecular_index(p, i)
                if index in gs.populations_to_show:
                    plot_molecule_population(ax_mol, i)

            describer_mol = 'Molecular populations'

            if 'polarisation' in gs.populations_to_show:
                if gs.linear_scale_for_polarisation:
                    ax_pho.semilogx(
                        x_axis, polarisation_degree(solutions),
                        label='$P_{out}$', color='r')
                else:
                    ax_pho.loglog(
                        x_axis, polarisation_degree(solutions),
                        label='$P_{out}$', color='r')
        output = []

        if 'n' in gs.populations_to_show:
            output.append(complete_graph(fig_pho, ax_pho, describer_pho,
                                         "pumping_response_photon_mode"))

        if 'N' in gs.populations_to_show:
            output.append(complete_graph(fig_mol, ax_mol, describer_mol,
                                         "pumping_response_excited_state"))

        return output


def diffusion_response(b, gs, p, solutionss):
    if gs.plot_diffusion == 'polarisation':
        describer = 'output polarisation degree'
        cap_describer = 'Output polarisation degree'
        underscore_describer = 'output_polarisation_degree'
        symbol = '$P_{out}$'
    elif 'N' in gs.plot_diffusion:
        describer = 'molecular populations'
        cap_describer = 'Molecular populations'
        underscore_describer = 'molecular_populations'
        symbol = '$N_{l, m}$'
    elif 'n' in gs.plot_diffusion:
        describer = 'photon populations'
        cap_describer = 'Photon populations'
        underscore_describer = 'photon_populations'
        symbol = '$n^{\sigma\sigma}_{\mathfrak{m}}$'

    b.print_log(f'\nPlotting {describer} against diffusion strength...\n')

    number = len(solutionss)

    fig, axs = _initialise_plot(gs, 1.3 * number / (2 * golden),
                                rows=number)

    if 'numpy.ndarray' not in str(type(axs)):
        axs = [axs]

    if number > 1:
        fig.subplots_adjust(hspace=0)

    if gs.diffusion_paper_format:

        axs[number - 1].set_xlabel('$D$ (GHz)')

        results = [[p.find_output_polarisation_degree(solution)
                    for solution in solutions]
                   for solutions in solutionss]

        for ax, result, gamma_up in zip(
                axs, results, p.gamma_up_diffusion_list):
            ax.set_ylabel('$P$')

            if p.diffusion_log:
                ax.semilogx(
                    p.diffusion_list, result,
                    label=r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                          f' = {gamma_up/p.gamma_down}',
                    color='k')
            else:
                ax.plot(
                    p.diffusion_list, result,
                    label=r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                          f' = {gamma_up/p.gamma_down}',
                    color='k')

            ax.set_xlim(p.diffusion_list[0], p.diffusion_list[-1])

        axs[0].legend(loc='best')
        axs[1].legend(loc='lower left')

        if not gs.diffusion_offset:
            axs[1].ticklabel_format(axis='y', useOffset=False)

        fudge = 0.08
        for ax, result in zip(axs, results):
            ax.set_ylim(
                min(result) - (fudge * (max(result) - min(result))),
                fudge * (max(result) - min(result)) + max(result))

        # axs[1].set_ylim(0.9995, 1.00005)
        axs[0].set_xticklabels([])

        # axs[0].locator_params(axis='y', nbins=7)
        # axs[1].locator_params(axis='y', nbins=7)

        return [(fig, 'polarisations_against_diffusion')]

    else:

        if gs.draw_graph_titles:
            axs[1].set_title(cap_describer + ' against diffusion strength')

        for ax in axs:
            ax.set_ylabel(symbol)

        axs[-1].set_xlabel('$D$ (GHz)')

        if gs.plot_diffusion == 'polarisation':

            results = [[p.find_output_polarisation_degree(solution)
                        for solution in solutions] for solutions in solutionss]

            for ax, gamma_up, result in \
                    zip(axs, p.gamma_up_diffusion_list, results):

                if p.diffusion_log:
                    ax.semilogx(
                        p.diffusion_list, result,
                        '.' if len(p.diffusion_list_log) < 5 else '-',
                        label=r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                              f' = {gamma_up/p.gamma_down}')
                else:
                    ax.plot(
                        p.diffusion_list, result,
                        label=r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                              f' = {gamma_up/p.gamma_down}')

                ax.legend(loc='best')

                if not gs.diffusion_offset:
                    if gamma_up > 0.01:
                        ax.ticklabel_format(axis='y', useOffset=False)

                if gamma_up < 0.01:
                    ax.set_ylim(min(result) * 0.99, max(result) * 1.01)

        elif 'N' in gs.plot_diffusion:

            def plot_molecule_population(
                    ax_, gamma_up_, i_, solutions_, multiplier):

                if multiplier != 1:
                    print('Warning: labels do not show multiplier!')

                solution = [
                    multiplier * element for element in
                    _extract_a_molecule_population(p, i_, solutions_)]

                if p.diffusion_log:
                    mol_label = _molecular_label(p, i_)

                    # top = 4
                    # values = [[[(l, sign * m) for m in range(2, l + 2, 2)]
                    #            for l in range(2, top + 2, 2)]
                    #           for sign in (-1, 1)]

                    for l, m in [(2, 2), (4, 2), (4, 4)]:
                        if mol_label == '$N_{' + f'{l}, {m}' + '}$':
                            mol_label = '$N_{' + f'{l}, \pm{m}' + '}$'
                    if solution[0] < 0:
                        mol_label = f'|{mol_label}|'
                        solution = [-x for x in solution]

                    ax_.loglog(
                        p.diffusion_list_log, solution,
                        label=mol_label,
                        linestyle=_index_line_style_cycler(i_))
                    ax_.set_xlim(p.diffusion_list_log[0],
                                 p.diffusion_list_log[-1])
                else:
                    ax_.plot(
                        p.diffusion_list_log, solution,
                        label=f'{_molecular_label(p, i_)} : ' +
                              r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                              f' = {gamma_up_/p.gamma_down}',
                        linestyle=_index_line_style_cycler(i_))
                    ax.set_xlim(p.diffusion_list_lin[0],
                                p.diffusion_list_lin[-1])

            for ax, gamma_up, solutions in \
                    zip(axs, p.gamma_up_diffusion_list, solutionss):
                for i in range(0, p.number_of_molecular_l_m_states()):

                    if gs.plot_diffusion == 'all_N':
                        plot_molecule_population(
                            ax, gamma_up, i, solutions, 1)

                    else:
                        index = _molecular_index(p, i)
                        if index in gs.plot_diffusion:
                            sign = -1 if '-' + index in \
                                         gs.plot_diffusion else 1

                            if index + '*' in gs.plot_diffusion:

                                string = gs.plot_diffusion

                                start = string.find(index) + len(index) + 1
                                end = string.find(', ', start)
                                factor = string[start:end]

                            else:

                                factor = '1'

                            plot_molecule_population(
                                ax, gamma_up, i, solutions,
                                sign * eval(factor))

                ax.legend(
                    loc='best',
                    title=r'$\Gamma_{\uparrow}/\Gamma_{\downarrow}$' +
                          f' = {gamma_up/p.gamma_down}')

        if 'n' in gs.plot_diffusion:

            def extract_a_photon_population(human_index):
                mode_number = human_index[:len(human_index) - 1]
                sigma_ = human_index[-1]
                return [row[_photon_mode_index(mode_number, sigma_)]
                        for row in solutions]

            def plot_pretty_photon_population(ax_pho, population_, i):
                if p.diffusion_log:
                    ax_pho.semilogx(
                        p.diffusion_list, population_,
                        label=_inverse_photon_mode_label(i),
                        linestyle=_line_style_cycler(i),
                        color=_colour_cycler(i))
                else:
                    ax_pho.plot(
                        p.diffusion_list, population_,
                        label=_inverse_photon_mode_label(i),
                        linestyle=_line_style_cycler(i),
                        color=_colour_cycler(i))

            for ax, gamma_up, solutions in \
                    zip(axs, p.gamma_up_diffusion_list, solutionss):

                for i in range(0, p.total_number_of_modes(p.number_of_modes)):
                    index = _inverse_photon_mode_index(i)
                    name = f'n{index}'
                    if name in gs.plot_diffusion:
                        plot_pretty_photon_population(
                            ax, extract_a_photon_population(index), i)

                ax.legend()

        for ax in axs[:-1]:
            ax.set_xticklabels([])

        return [(fig, underscore_describer + '_against_diffusion')]


def two_dimensional_map_polarisation(b, gs, p, solutions):
    b.print_log(
        '\nPlotting two dimensional colour map of output polarisation '
        'against input polarisation and pumping strength...\n')

    fig, ax = _initialise_plot(gs, 0.75)

    if gs.draw_graph_titles:
        ax.set_title(gs.polarisation_colourmap_title)

    ax.set_xlabel('$\Gamma_{\\uparrow} / \Gamma_{\\downarrow}$')
    ax.set_ylabel('$P_{pump}$')

    x_data = \
        [gamma_up / p.gamma_down for gamma_up in p.gamma_up_linear_list]

    if p.pump_polarisation_definition == 'angle':
        y_data = [p.polarisation_degree(i)
                  for i in p.input_polarisation_list]
    elif p.pump_polarisation_definition == 'degree':
        y_data = p.pump_polarisation_list

    output_polarisation_degree_list_half = \
        [[p.find_output_polarisation_degree(solution)
          for solution in line] for line in solutions]

    # if False:
    #
    #     from scipy.interpolate import interp2d
    #
    #     y_half = p.input_polarisation_list_half
    #
    #     print(len(x_data))
    #     print(len(y_half))
    #     print(len(output_polarisation_degree_list_half))
    #     print(len(output_polarisation_degree_list_half[0]))
    #
    #     xx, yy = meshgrid(x_data, y_half)
    #
    #     f = interp2d(xx, yy,
    #                  output_polarisation_degree_list_half)
    #     exit()
    #     scale = 2
    #
    #     x_min = x_data[0]
    #     x_max = x_data[-1]
    #     x_step = abs(x_data[1] - x_min)
    #
    #     y_min = y_data[0]
    #     y_max = y_data[-1]
    #     y_step = y_data[1] - y_min
    #
    #     y_min_half = y_half[0]
    #     y_max_half = y_half[-1]
    #     y_step_half = abs(y_half[1] - y_min_half)
    #
    #     x_interp = arange(x_min, x_max, x_step * 1 / scale)
    #     y_interp = arange(y_min_half, y_max_half, y_step_half * 1 / scale)
    #     z_interp = f(x_interp, y_interp)
    #
    #     x_data = x_interp
    #     y_data = arange(y_min, y_max, y_step * 1 / scale)
    #     output_polarisation_degree_list_half = z_interp

    output_polarisation_degree_list = \
        [[-el for el in line]
         for line in output_polarisation_degree_list_half][::-1] + \
        output_polarisation_degree_list_half

    if gs.contour:
        im = ax.contourf(
            x_data, y_data, output_polarisation_degree_list,
            gs.contour_levels)

    else:

        x_min = x_data[0]
        x_max = x_data[-1]
        x_step = abs(x_data[1] - x_min)

        y_min = y_data[0]
        y_max = y_data[-1]
        y_step = y_data[1] - y_min

        new_x_min = x_min - 0.5 * x_step
        new_x_max = x_max + 0.5 * x_step
        new_y_min = y_min - 0.5 * y_step
        new_y_max = y_max + 0.5 * y_step

        z = output_polarisation_degree_list

        y_ = arange(new_y_min,
                    new_y_max + (y_step if len(y_data) < 10 else 0),
                    y_step)

        if gs.interpolate:
            b.print_log('Interpolating data...\n')

            from scipy.interpolate import interp1d

            z_interp = []

            for i, (y, out) in enumerate(zip(y_data, z)):
                f = interp1d(x_data, out, kind=5)

                x_interp = arange(x_min, x_max,
                                  x_step * 1 / gs.interp_scale)

                out_interp = f(x_interp)

                z_interp.append(out_interp)

            im = ax.pcolormesh(x_interp, y_, z_interp, vmin=-1, vmax=1)

        else:

            x_ = arange(new_x_min, new_x_max, x_step)
            x, y = meshgrid(x_, y_)

            # if True:
            #     from matplotlib.colors import LogNorm
            #     z = [[x + 1 for x in l] for l in z]
            #     flat = [x for l in z for x in l]
            #
            #     z = z[:len(z)//2]
            #     print(z)
            #     im = ax.pcolormesh(
            #         x, y, z, norm=LogNorm(vmin=min(flat), vmax=2))
            #
            # else:
            im = ax.pcolormesh(x, y, z, vmin=-1, vmax=1)

        ax.set_xlim(new_x_min, new_x_max)
        ax.set_ylim(-1, 1)

    colourbar = fig.colorbar(im)
    colourbar.set_label('$P$')
    colourbar.locator = MaxNLocator(nbins=5)
    colourbar.update_ticks()

    b.print_log('Graphing complete.\n')

    return [(fig, "2d_colour_map_P")]


def save_to_disk(b, figure_, name):
    figure_.savefig(name, bbox_inches='tight', format='pdf',
                    transparent=True, frameon=False)
    b.print_log(f'  Saved: "{name}".\n')


if __name__ == '__main__':
    exit()
