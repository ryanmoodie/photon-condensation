#!/usr/bin/env python
# coding=utf-8

"""
Settings.py
Reads parameter values from .cfg files, loads values from
Mathematica/Fortran-generated tables for integrals and gamma terms,
calculates some values and contains population indexing functions.
"""

from configparser import RawConfigParser
from csv import reader
from datetime import datetime
from math import cos, sin, radians
from math import sqrt, pi
from multiprocessing import cpu_count
from pickle import dump, load
from platform import system as operating_system
from time import clock

from numpy import logspace, log10, linspace
from scipy.misc import factorial

__author__ = "Ryan Moodie"
__email__ = "rim2@st-andrews.ac.uk"
__date__ = "24 Jun 2016"


class ProgramSettings:
    def __init__(self, settings_file_suffix=''):
        program = RawConfigParser()

        self.settings_file_suffix = settings_file_suffix
        program.read(f"settings/program{self.settings_file_suffix}.cfg")

        self.server_mode = program.getboolean('QuickSettings', 'server_mode')
        self.testing_mode = program.getboolean('QuickSettings', 'testing_mode')

        if self.server_mode:
            self.verbose = True
            self.logging = True
            self.sound_alarm = False

        elif self.testing_mode:
            self.verbose = True
            self.logging = False
            self.sound_alarm = True

        else:
            self.verbose = program.getboolean('ProgramOptions', 'verbose')
            self.logging = program.getboolean('ProgramOptions', 'logging')
            self.sound_alarm = program.getboolean('CompletionAlarm', 'sound')

        _data_file_name = program.get('OutputFileNames', 'data_file')
        _log_file_name = program.get('OutputFileNames', 'log_file')

        self.log_file = f'log/{_log_file_name}-{_data_file_name}.log'

        if self.verbose:
            self.program_start_time = clock()
            self.print_log(f'\nStart time: {datetime.now()}'
                           f'\n  Program initialising...\n    '
                           f'Simulation name: "{_data_file_name}".\n')

        self.population_distribution = program.getboolean(
            'ProgramActions', 'spectrum')
        self.time_evolution = program.getboolean(
            'ProgramActions', 'time_evolution')
        self.steady_state = program.getboolean(
            'ProgramActions', 'steady_state')
        self.pumping_response = program.getboolean(
            'ProgramActions', 'pumping_response')
        self.diffusion_response = program.getboolean(
            'ProgramActions', 'diffusion_response')
        self.two_d_map = program.getboolean(
            'ProgramActions', 'two_d_map_P')

        _data_file = f'temp/{_data_file_name}'
        _data_file_suffix = '.pkl'

        self.label_time_evolution = \
            f'{_data_file}_time_evolution{_data_file_suffix}'
        self.label_spectrum = \
            f'{_data_file}_spectrum{_data_file_suffix}'
        self.label_pump_response = \
            f'{_data_file}_pumping_response{_data_file_suffix}'
        self.label_diffusion = \
            f'{_data_file}_diffusion{_data_file_suffix}'
        self.label_2d_map = \
            f'{_data_file}_2d_map{_data_file_suffix}'
        self.label_2d_map_D = \
            f'{_data_file}_2d_map_D{_data_file_suffix}'

        _graph_identifier = _data_file_name if program.getboolean(
            'OutputFileNames', 'name_graph_after_data') else program.get(
            'OutputFileNames', 'graph_output_file')

        self.graph_name = f'graphs/{_graph_identifier}_'

        self.print_solutions = program.getboolean('Testing', 'print_solutions')

        self.print_system_parameters = \
            program.getboolean('Testing', 'print_system_parameters')

        _number_of_workers = program.get('Parallel', 'number_of_workers')

        self.number_of_workers = cpu_count() if _number_of_workers == '' \
            else int(_number_of_workers)

        self.alarm_timeout = \
            program.getint('CompletionAlarm', 'only_play_after')

        _gamma_file_name = program.get('InputFileNames', 'gamma_values')
        _integral_file_name = program.get('InputFileNames', 'integral_values')
        _capital_a_file_name = \
            program.get('InputFileNames', 'capital_a_values')

        self.gamma_file = f'data/{_gamma_file_name}'
        self.integral_file = f'data/{_integral_file_name}.csv'
        self.capital_a_file = f'data/{_capital_a_file_name}.pkl'

        self.l_max_for_pre_calculation = \
            program.getint('PreCalculation', 'l_max')

        self.do_capital_a_pre_calculation = program.getboolean(
            'PreCalculation', 'do_capital_a_pre_calculation')

        self.integrate_to_steady_step = program.getint(
            'IterativeIntegration', 'iteration_time_step')
        self.integrate_to_steady_precision = program.getfloat(
            'IterativeIntegration', 'convergence_precision')
        self.zero = program.getfloat('Solver', 'zero')
        self.integration_tolerance = program.getfloat(
            'IterativeIntegration', 'integration_tolerance')
        self.cut_off_integrator_at_zero = program.getboolean(
            'IterativeIntegration', 'cut_off_at_zero')

        self.tolerance = program.getfloat('RootFinder', 'tolerance')
        self.cut_off_root_finder_at_zero = \
            program.getboolean('RootFinder', 'cut_off_at_zero')
        self.check_physicality = \
            program.getboolean('RootFinder', 'check_physicality')

    def raw_read_file(self, filename):
        if self.verbose:
            self.print_log(f'Reading data from file: "{filename}".')
        with open(filename, 'rb') as input_file:
            return load(input_file)

    def display_system_parameters(self, s_):
        if self.verbose and self.print_system_parameters:
            self.print_log(
                f'\n  System parameters are (ignore irrelevant):\n'
                f'    l_max = {s_.l_max}\n'
                f'    number_of_modes = {s_.number_of_modes}\n'
                f'    kappa = {s_.kappa} GHz\n'
                f'    N = {s_.capital_n} molecules\n'
                f'    D = {s_.capital_d} GHz (unless list)\n'
                f'    gamma_up = {s_.gamma_up} GHz (unless list)\n'
                f'    gamma_up log list: {s_.gamma_up_list_number}\n'
                f'    D list len: {s_.diffusion_list_number}\n'
                f'    gamma_down = {s_.gamma_down} GHz\n'
                f'    gamma_plus x {s_.gamma_plus_multiplier}\n'
                f'    gamma_minus x {s_.gamma_minus_multiplier}\n'
                f'    input_polarisation = {s_.input_polarisation}'
                f' [for (x, y)] (unless list)')

    def read_file(self, filename):
        s, solution = self.raw_read_file(filename)

        self.display_system_parameters(s)

        if self.print_solutions:
            self.print_log('\n  Solutions:')
            for sol in solution:
                self.print_log(f'\n{sol}')

        return s, solution

    def write_out(self, filename, data):
        if self.verbose:
            self.print_log(f'Saving data to file: "{filename}".\n')
        with open(filename, 'wb') as output_file:
            dump(data, output_file)

    def print_log(self, text):
        if self.verbose:
            print(text)
        if self.logging:
            with open(self.log_file, 'a') as file:
                file.write(f'{text}\n')

    @staticmethod
    def raw_stopwatch(start_time):
        return round(clock() - start_time, 2)

    def stopwatch(self, start_time):
        return self.time_formatter(self.raw_stopwatch(start_time))

    @staticmethod
    def time_formatter(total_seconds__):
        def truncate(i):
            return str(int(round(i, 0)))

        total_minutes__, quotient_seconds__ = divmod(total_seconds__, 60)
        hours__, quotient_minutes__ = divmod(total_minutes__, 60)

        total_seconds_ = str(total_seconds__)
        quotient_seconds_ = truncate(quotient_seconds__)
        total_minutes_ = truncate(total_minutes__)
        quotient_minutes_ = truncate(quotient_minutes__)
        hours_ = truncate(hours__)

        total_seconds_unit = ' second' if total_seconds__ == 1 else ' seconds'
        quotient_seconds_unit = \
            ' second' if quotient_seconds_ == '1' else ' seconds'
        total_minutes_unit = ' minute' if total_minutes_ == '1' else ' minutes'
        quotient_minutes_unit = \
            ' minute' if quotient_minutes_ == '1' else ' minutes'
        hours_unit = ' hour' if hours_ == '1' else ' hours'

        total_seconds = total_seconds_ + total_seconds_unit
        quotient_seconds = quotient_seconds_ + quotient_seconds_unit
        total_minutes = total_minutes_ + total_minutes_unit
        quotient_minutes = quotient_minutes_ + quotient_minutes_unit
        hours = hours_ + hours_unit

        if hours__ >= 1:
            return f'{hours}, {quotient_minutes} and {quotient_seconds}'

        elif total_minutes__ >= 1:
            return f'{total_minutes} and {quotient_seconds}'

        else:
            return total_seconds

    def completion_dialogue(self):
        def completion_alarm():

            if operating_system() == 'Windows':
                from winsound import Beep

                def arpeggio():
                    length = 200
                    Beep(262, length)
                    Beep(330, length)
                    Beep(392, length)
                    Beep(523, 2 * length)

                arpeggio()

            elif operating_system() == 'Linux':
                from os import system

                system('play --no-show-progress --null '
                       '--channels 1 synth %s sine %f' % (0.1, 1000))

        if self.verbose:
            self.print_log(
                f'Program complete.\n'
                f'  Run time: {self.stopwatch(self.program_start_time)}.\n'
                f'    End time: {datetime.now()}\n')

            if self.sound_alarm and self.raw_stopwatch(
                    self.program_start_time) > self.alarm_timeout:
                completion_alarm()

            self.print_log(
                '\n--------------------------------------------------'
                '----------------------\n')


class Parameters:
    def __init__(self, b):
        parameters = RawConfigParser()
        parameters.read(f"settings/parameters{b.settings_file_suffix}.cfg")

        if b.server_mode:
            self.l_max = 6
            self.number_of_modes = 35

        elif b.testing_mode:
            self.l_max = 0
            self.number_of_modes = 1

        else:
            self.l_max = parameters.getint('TruncationSettings', 'l_max')
            if self.l_max % 2 == 1:
                exit('l_max must be even!')
            self.number_of_modes = \
                parameters.getint('TruncationSettings', 'number_of_modes')

        if b.do_capital_a_pre_calculation:
            self.populate_capital_a_dictionary(b)

        self.gamma_plus_multiplier = parameters.getfloat(
            'SystemParameters', 'gamma_plus_multiplier')
        self.gamma_minus_multiplier = parameters.getfloat(
            'SystemParameters', 'gamma_minus_multiplier')

        self.gamma_plus_list, self.gamma_minus_list = \
            self._gamma_values(b.gamma_file)

        self.integral_values_dictionary = \
            self._integral_values(b.integral_file)

        self.gamma_down = parameters.getfloat('SystemParameters', 'gamma_down')
        self.kappa = parameters.getfloat('SystemParameters', 'kappa')
        self.capital_d = parameters.getfloat('SystemParameters', 'D')
        self.capital_n = parameters.getfloat('SystemParameters', 'N')

        self.gamma_up = parameters.getfloat('PumpingStrength', 'gamma_up')

        _gamma_up_diffusion_list = \
            parameters.get('Diffusion', 'gamma_up_values')

        self.gamma_up_diffusion_list = [self.gamma_up] if \
            _gamma_up_diffusion_list == '' else \
            eval(_gamma_up_diffusion_list)

        _gamma_up_list_start = parameters.getfloat(
            'PumpingStrength', 'gamma_up_list_start')
        _gamma_up_list_end = parameters.getfloat(
            'PumpingStrength', 'gamma_up_list_end')

        _gamma_up_step = parameters.getfloat('PumpingStrength', 'step')

        self.gamma_up_list_number = \
            int((_gamma_up_list_end - _gamma_up_list_start) // _gamma_up_step)

        self.gamma_up_log_list = logspace(
            log10(_gamma_up_list_start), log10(_gamma_up_list_end),
            self.gamma_up_list_number)

        self.gamma_up_linear_list = linspace(
            _gamma_up_list_start, _gamma_up_list_end,
            self.gamma_up_list_number)

        diffusion_list_start = parameters.getfloat(
            'Diffusion', 'diffusion_list_start')
        diffusion_list_end = parameters.getfloat(
            'Diffusion', 'diffusion_list_end')

        self.diffusion_list_number = parameters.getint(
            'Diffusion', 'diffusion_list_number_plot')

        self.diffusion_log = \
            parameters.getboolean('Diffusion', 'diffusion_log')

        if self.diffusion_log:
            if not diffusion_list_start:
                exit("0 doesn't make sense as start of log scale!")
            self.diffusion_list = logspace(
                log10(diffusion_list_start), log10(diffusion_list_end),
                self.diffusion_list_number)
        else:
            self.diffusion_list = linspace(
                diffusion_list_start, diffusion_list_end,
                self.diffusion_list_number)

        self.pump_polarisation_definition = parameters.get(
            'PumpPolarisation', 'define_pump_polarisation_by')

        if self.pump_polarisation_definition == 'angle':
            self.pump_polarisation_angle = parameters.getfloat(
                'PumpPolarisation', 'pump_polarisation_angle')

            self.input_polarisation = \
                self._polarisation_contributions_from_angle(
                    self.pump_polarisation_angle)

        elif self.pump_polarisation_definition == 'degree':
            self.pump_polarisation_degree = parameters.getfloat(
                'PumpPolarisation', 'pump_polarisation_degree')

            self.input_polarisation = \
                self._polarisation_contributions_from_degree(
                    self.pump_polarisation_degree)

        self.pump_polarisation_list_number = parameters.getint(
            'PumpPolarisation', 'pump_polarisation_list_number')

        if self.pump_polarisation_definition == 'angle':
            _contributions = self._polarisation_contributions_from_angle

        elif self.pump_polarisation_definition == 'degree':
            _contributions = self._polarisation_contributions_from_degree

        _pump_polarisation_list_start = parameters.getfloat(
            'PumpPolarisation',
            f'pump_polarisation_{self.pump_polarisation_definition}_list_start')
        _pump_polarisation_list_end = parameters.getfloat(
            'PumpPolarisation',
            f'pump_polarisation_{self.pump_polarisation_definition}_list_end')

        self.pump_polarisation_list = linspace(
            _pump_polarisation_list_start, _pump_polarisation_list_end,
            self.pump_polarisation_list_number)

        self.input_polarisation_list = \
            [_contributions(pump_polarisation) for pump_polarisation
             in self.pump_polarisation_list]

        if self.pump_polarisation_list_number % 2:
            exit('Use even number for polarisation list number.')

        pump_polarisation_list_half_number = \
            self.pump_polarisation_list_number // 2

        self.input_polarisation_list_half = \
            self.input_polarisation_list[pump_polarisation_list_half_number:
            self.pump_polarisation_list_number]

        self.input_initial_state = \
            parameters.get('SystemParameters', 'initial_state')

        self.stop_time = parameters.getfloat('TimeEvolution', 'stop_time')
        self.number_of_points = self.stop_time * 20

        self.cap_a = b.raw_read_file('data/capital_a_l_m.pkl')

        self.spectrum_gamma_up_list = \
            eval(parameters.get('Spectrum', 'gamma_up_list'))

        self.spectrum_modes = \
            parameters.getint('Spectrum', 'number_of_modes')

    @staticmethod
    def total_number_of_modes(number_of_energy_modes):
        return 2 * number_of_energy_modes

    def number_of_molecular_l_m_states(self):
        return sum([(2 * i + 1) for i in range(0, int(self.l_max / 2) + 1)])

    def total_number_of_states(self, number_of_energy_modes):
        return self.total_number_of_modes(number_of_energy_modes) + \
               self.number_of_molecular_l_m_states()

    def initial_state(self, number_of_energy_modes):
        return [0.] * self.total_number_of_states(number_of_energy_modes) \
            if self.input_initial_state == 'empty' else \
            eval(self.input_initial_state)

    def molecule_index_dict(self, photon_modes):
        return {
            (l, m): self._molecule_index(l, m, photon_modes)
            for l in range(0, self.l_max + 1, 2) for m in range(- l, l + 1, 2)}

    @staticmethod
    def polarisation_degree(vector):
        x, y = vector
        if not (x + y):
            return 0
        return (x - y) / (x + y)

    def find_output_polarisation_degree(self, solution):
        x = sum([solution[i] for i in range(
            0, self.total_number_of_modes(self.number_of_modes), 2)])
        y = sum([solution[i] for i in range(
            1, self.total_number_of_modes(self.number_of_modes) + 1, 2)])

        return self.polarisation_degree((x, y))

    @staticmethod
    def _polarisation_contributions_from_angle(angle_degrees):
        if angle_degrees > 360 or angle_degrees < 0:
            exit('Polarisation angle must be between 0 and 360 degrees!')
        angle_radians = radians(angle_degrees)
        x = (cos(angle_radians)) ** 2
        y = (sin(angle_radians)) ** 2
        return x, y

    @staticmethod
    def _polarisation_contributions_from_degree(degree):
        if degree == 1:
            return 1, 0
        else:
            x = (1 + degree) / (1 - degree)
            y = 1
            total = x + y
            return x / total, y / total

    def gamma_plus(self, i_):
        return self.gamma_plus_list[self.photon_mode_a(i_)]

    def gamma_minus(self, i_):
        return self.gamma_minus_list[self.photon_mode_a(i_)]

    @staticmethod
    def _integral_values(filename):
        with open(filename, 'r') as file:
            integral_values_list = list(reader(file))

        # (l1, m1, l2, m2): integral_value
        integral_values_dictionary = {
            (int(row[0]), int(row[1]), int(row[2]), int(row[3])):
                float(row[4]) for row in integral_values_list}

        return integral_values_dictionary

    def _gamma_values(self, filename):
        with open(filename, 'r') as file:
            raw_list = list(reader(file, delimiter=' '))[1:]

        gamma_values_list = [
            [float(element) for element in row if element is not '']
            for row in raw_list]

        gamma_plus_list = [
            self.gamma_plus_multiplier * row[0] for row in gamma_values_list]
        gamma_minus_list = [
            self.gamma_minus_multiplier * row[1] for row in gamma_values_list]

        return gamma_plus_list, gamma_minus_list

    @staticmethod
    def photon_mode_a(i_):
        return i_ // 2

    def _molecule_index(self, l, m, photon_modes):
        return int(
            1 + self.total_number_of_modes(photon_modes) + 0.5 * (l + m) +
            sum([(2 * n_ - 1) for n_ in range(0, int(l / 2) + 1)]))

    @staticmethod
    def inverse_molecule_index(n):
        i_ = 0
        while n > (2 * i_ - 1):
            n -= (2 * i_ - 1)
            i_ += 1

        l = (i_ - 1) * 2
        m = (n - i_) * 2

        return l, m

    def populate_capital_a_dictionary(self, b):
        def generate_capital_a(l__, m__):
            return sqrt(
                ((2 * l__ + 1) * factorial(l__ - m__)) /
                ((4 * pi) * factorial(l__ + m__)))

        if b.verbose:
            start_time = clock()
            b.print_log('Calculating A_{l,m} values...')

        cap_a = {(l, m): generate_capital_a(l, m)
                 for l in range(0, self.l_max_for_pre_calculation + 1, 2)
                 for m in range(- l, l + 1, 2)}

        b.write_out(b.capital_a_file, cap_a)

        if b.verbose:
            b.print_log(f'Done ({round(clock() - start_time, 3)}s)')


class GraphSettings:
    def __init__(self, b):
        graphs = RawConfigParser()
        graphs.read(f'settings/graphs{b.settings_file_suffix}.cfg')

        if b.server_mode:
            self.show_graphs = False
            self.save_graph_to_disk = True

        elif b.testing_mode:
            self.show_graphs = True
            self.save_graph_to_disk = False

        else:
            self.save_graph_to_disk = graphs.getboolean(
                'GraphOptions', 'save_graphs_to_disk')
            self.show_graphs = graphs.getboolean(
                'GraphOptions', 'show_graphs')

        self.reload_parameters_when_graphing = graphs.getboolean(
            'GraphOptions', 'reload_parameters_when_graphing')

        self.use_old_graph_aesthetic = graphs.getboolean(
            'GraphOptions', 'use_old_graph_aesthetic')

        self.only_load_values = graphs.getboolean(
            'GraphOptions', 'only_load_values')

        self.picture_number = 0
        self.graph_suffix = '.pdf'
        self.graph_number = 0

        self.figure_width = graphs.getfloat(
            'GraphOptions', 'figure_width')

        self.diffusion_paper_format = \
            graphs.getboolean('DiffusionResponse', 'paper_format')

        self.plot_diffusion = 'polarisation' if self.diffusion_paper_format \
            else graphs.get('DiffusionResponse', 'plot')

        self.populations_to_show = graphs.get(
            'PumpingResponse', 'populations_to_show')

        self.linear_scale_for_polarisation = graphs.getboolean(
            'PumpingResponse', 'linear_scale_for_polarisation')
        self.linear_scale_for_photons = graphs.getboolean(
            'PumpingResponse', 'linear_scale_for_photons')
        self.pumping_log_scale_for_molecules = graphs.getboolean(
            'PumpingResponse', 'log_scale_for_molecules')

        self.paper_format_fig_3 = graphs.getboolean(
            'PumpingResponse', 'paper_format')
        self.paper_polarisation = graphs.getboolean(
            'PumpingResponse', 'paper_polarisation')

        self.diffusion_offset = \
            graphs.getboolean('DiffusionResponse', 'offset')

        self.plot = graphs.get('TimeEvolution', 'plot')
        self.linear_photons = graphs.get('TimeEvolution', 'linear_photons')

        self.draw_graph_titles = graphs.getboolean(
            'GraphOptions', 'draw_graph_titles')
        self.custom_graph_aspect = graphs.getboolean(
            'GraphOptions', 'custom_graph_aspect')

        self.polarisation_colourmap_title = graphs.get(
            'PolarisationColourmap', 'title')

        self.contour = graphs.getboolean(
            'PolarisationColourmap', 'contour')
        self.contour_levels = graphs.getint(
            'PolarisationColourmap', 'contour_levels')
        self.interpolate = graphs.getboolean(
            'PolarisationColourmap', 'interpolate')
        self.interp_scale = graphs.getint(
            'PolarisationColourmap', 'interp_scale')

        self.aspect_time = graphs.getfloat('TimeEvolution', 'aspect')

    def plural_check(self):
        return 's' if self.graph_number > 1 else ''


if __name__ == '__main__':
    exit()
