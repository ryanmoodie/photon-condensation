Photon Bose-Einstein condensation (BEC) model including effects of
polarisation.

Python program to solve the rate equations (coupled differential
equations) that describe the photon BEC system
including effects of polarisation.

Python version: 3.6+

Packages required:
Numpy
Scipy
Matplotlib
